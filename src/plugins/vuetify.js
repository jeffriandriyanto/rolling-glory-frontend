import Vue from 'vue';
import Vuetify from 'vuetify';
import 'vuetify/dist/vuetify.min.css';

Vue.use(Vuetify);

export default new Vuetify({
  theme: {
    dark: false,
    themes: {
      light: {
        primary: '#006A4E',
        secondary: '#D5E8C6',
        accent: '#838EAB',
        error: '#E64580',
        info: '#2196F3',
        success: '#74B71B',
        warning: '#F0D946',
        grey: '#3C3C3F',
      },
      dark: {
        primary: '#006A4E',
        secondary: '#D5E8C6',
        accent: '#838EAB',
        error: '#E64580',
        info: '#2196F3',
        success: '#74B71B',
        warning: '#F0D946',
        grey: '#3C3C3F',
      },
    },
  },
});
