import request from '@/request';

export function getProduct() {
  return request({
    method: 'get',
    url: '/gifts',
  });
}

export function getProductDetail(id) {
  return request({
    method: 'get',
    url: `/gifts/${id}`,
  });
}

export function setWhislist(id) {
  return request({
    method: 'post',
    url: `/gifts/${id}/wishlist`,
  });
}
