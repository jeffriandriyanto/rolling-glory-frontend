const app = {
  state: {
    whislist: [],
  },
  mutations: {
    SET_WHISLIST: (state, value) => {
      const idx = state.whislist.indexOf(value);
      if (idx === -1) {
        state.whislist.push(value);
      } else {
        state.whislist.splice(idx, 1);
      }
      localStorage.setItem('whislist', state.whislist);
    },
  },
};

export default app;
