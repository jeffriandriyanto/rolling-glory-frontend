import Vue from 'vue';
import VueRouter from 'vue-router';
import ProductList from '@/views/ProductList.vue';
import ProductDetail from '@/views/ProductDetail.vue';
import FourOFour from '@/components/404_page.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'ProductList',
    component: ProductList,
  },
  {
    path: '/product-detail/:id',
    name: 'ProductDetail',
    component: ProductDetail,
  },
  { path: '/:pathMatch(.*)*', component: FourOFour },
];

const router = new VueRouter({
  // mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

export default router;
