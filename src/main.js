import Vue from 'vue';
import VueProgressBar from 'vue-progressbar';

import App from './App.vue';
import './registerServiceWorker';
import router from './router';
import store from './store';
import vuetify from './plugins/vuetify';

Vue.use(VueProgressBar, {
  color: '#006A4E',
  failedColor: '#E64580',
  thickness: '5px',
});

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  vuetify,
  render: (h) => h(App),
}).$mount('#app');
