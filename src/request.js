import axios from 'axios';

// create an axios instance
const service = axios.create({
  baseURL: process.env.VUE_APP_BASE_API, // api of base_url
  timeout: 1000000, // timeout default
});

// request interceptor
service.interceptors.request.use(
  (config) => {
    const conf = config;
    return conf;
  },
  (error) => {
    Promise.reject(error);
  },
);

// response interceptor
service.interceptors.response.use(
  (response) => response,
  // eslint-disable-next-line consistent-return
  (error) => {
    const e = error;
    if (e && e.response) {
      const r = e.response;
      console.error(r.data.message);
      return Promise.reject(e);
    }
    console.error(e);
  },
);

export default service;
