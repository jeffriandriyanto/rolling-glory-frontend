# rolling-glory-fe-jeffri

## Project setup
> Initial Project
`npm install` or `yarn`

> Compiles and hot-reloads for development:
`npm run serve` or `yarn serve`

> Compiles and minifies for production:
`npm run build` or `yarn build`

> Use dist and upload to server to run static page

## Tech Spec
> **Vue.Js**
  Mengapa menggunakan Vue.Js karena menurut saya untuk project yang membutuhkan kecepatan pengerjaan vue paling mudah untuk dipelajari dan digunakan. ada 3 komponen utama di dalam file .vue (html, js, css) jadi menurut saya lebih mudah dipahami juga dan lebih mudah dalam pengaplikasiannya.

> **Framework: vue-cli**
  Vue Cli sangat membantu dalam proses awal pembuatan project, disitu sudah disediakan pilihan seperti, vue-router, vuex, pwa, linter jadi memudahkan saya untuk memudahkan memulai sebuah project.

> **Framework UI: Vuetify**
  Vuetify menurut saya sangat lengkap, frameworknya stabil karena sampai sekarang masih terus dikembangkan untuk update fitur dan bugs sehingga lebih aman untuk aplikasi yang sedang saya kerjakan, banyak kontributor, open source dan konsep desainnya juga menggunakan material desain, sehingga lebih mudah untuk user menggunakan dan desain material banyak user sudah sering melihatnya.

## Penjelasan
- [x] Aplikasi sudah mengimplementasikan Responsive View
- [x] Api untuk update whistlist tidak merubah status ketika get list lagi. sehingga saya simpan di localstorage agar bisa diakses di semua routing
- [x] Aplikasi sudah mengimplementasikan PWA
- [x] List dari product dapat difilter (Kombinasi)